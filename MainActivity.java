package indglobal.com.softsoon;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public ArrayList<Integer> ImagesArray = new ArrayList<>();
    public ArrayList<Integer> IMAGES = new ArrayList<>();
    public static ArrayList<String> IntroTitle = new ArrayList<>();
    public static ArrayList<String> IntroSubTitle = new ArrayList<>();

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);


        IMAGES.add(R.drawable.a);
        IMAGES.add(R.drawable.b);
        IMAGES.add(R.drawable.c);
        IMAGES.add(R.drawable.d);
        IMAGES.add(R.drawable.g);
        IMAGES.add(R.drawable.f);
        //  IMAGES.add(R.drawable.intro_four);

        IntroTitle.add(getResources().getString(R.string.intro_one_title));
        IntroTitle.add(getResources().getString(R.string.intro_two_title));
        IntroTitle.add(getResources().getString(R.string.intro_three_title));
        IntroTitle.add(getResources().getString(R.string.intro_four_title));
        IntroTitle.add(getResources().getString(R.string.intro_five_title));
        IntroTitle.add(getResources().getString(R.string.intro_six_title));



        IntroSubTitle.add(getResources().getString(R.string.intro_one_sub));
        IntroSubTitle.add(getResources().getString(R.string.intro_two_sub));
        IntroSubTitle.add(getResources().getString(R.string.intro_three_sub));
        IntroSubTitle.add(getResources().getString(R.string.intro_four_sub));
        IntroSubTitle.add(getResources().getString(R.string.intro_five_sub));
        IntroSubTitle.add(getResources().getString(R.string.intro_six_sub));

        init();

        viewPager = (ViewPager) findViewById(R.id.viewpager_mycontact);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs_mycontact);
        tabLayout.setupWithViewPager(viewPager);
        createTabIcons();

    }
    private void init() {

        for(int i=0;i<IMAGES.size();i++)
            ImagesArray.addAll(IMAGES);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new IntroSliderAdapter(MainActivity.this, ImagesArray));
        mPager.setPageTransformer(true, new flipCard());

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

        indicator.setFillColor(getResources().getColor(R.color.white));
        indicator.setStrokeColor(getResources().getColor(R.color.white));
        indicator.setRadius(5 * density);

        NUM_PAGES = IMAGES.size();

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 5000, 5000);

        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }
    private class flipCard implements ViewPager.PageTransformer {
        @Override
        public void transformPage(View page, float position) {

            final float rotation = 180f * position;

            page.setAlpha(rotation > 90f || rotation < -90f ? 0 : 1);
            page.setPivotX(page.getWidth() * 0.5f);
            page.setPivotY(page.getHeight() * 0.5f);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                page.setScrollBarFadeDuration(6000);
            }
            page.setRotationY(rotation);
        }


    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_Video(),"VIDEOS");
        adapter.addFragment(new Fragment_Image(),"IMAGES");
        adapter.addFragment(new Fragment_Milestone(),"MILESTONE");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            return ViewPagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void createTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_name, null);
        tabOne.setText("VIDEOS");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.videos, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_name, null);
        tabTwo.setText("IMAGES");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.images, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_name, null);
        tabThree.setText("MILESTONE");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.mile, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
